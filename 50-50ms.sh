sudo tc qdisc del dev ens5 root
sudo tc qdisc add dev ens5 root netem delay 50ms

echo "Fix 50 Latency 50ms"

for i in 1 2 3 
do
 START=$(date +%s.%N)
 ./migrator.sh > 50-50ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate fix 50 latency 50ms" $DIFF
 echo "Finish migration"
 echo $i " Time fix 50 latency 50ms" $DIFF  >> 50-50ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelost50-50ms-$i.txt
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "find data/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum >> checksum-fix50.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "rm data/*"
done
