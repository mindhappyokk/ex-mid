sudo tc qdisc del dev ens5 root
sudo tc qdisc add dev ens5 root netem delay 100ms
echo "Enhanced Latency 100 ms"

for i in 1 2 3 
do
 START=$(date +%s.%N)
 ./migrator.sh > enhance-50ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate enhance 100ms latency" $DIFF
 echo "Finish migration"
 echo $i " Time enhance 100ms latency" $DIFF  >> enhance-100ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelostenhand-100ms-$i.txt
# ssh suchanat@flynn.sci.tu.ac.th "rm data/*"
 ssh -i kubernetes_virginia.pem	ubuntu@172.31.2.60 "nolimit $i >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "find data/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "rm data/*"
done
