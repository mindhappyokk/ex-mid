sudo tc qdisc del dev ens5 root
sudo tc qdisc add dev ens5 root netem delay 100ms

for i in 1 2 3 
do
 START=$(date +%s.%N)
 ./migrator.sh > 1-100ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate Fix 1 latency 100ms" $DIFF
 echo "Finish migration"
 echo $i " Time Fix 1 latency 100ms" $DIFF  >> 1-100ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelost1-100ms-$i.txt
done
