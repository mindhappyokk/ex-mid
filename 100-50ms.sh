sudo tc qdisc del dev ens5 root
sudo tc qdisc add dev ens5 root netem delay 50ms

echo "Fix 100 Latency 50ms"

for i in 1 2 3 
do
 START=$(date +%s.%N)
 ./migrator.sh > 100-50ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate fix 100 latency 50ms" $DIFF
 echo "Finish migration"
 echo $i " Time fix 100 latency 50ms" $DIFF  >> 100-50ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelost100-50ms-$i.txt
 ssh -i kubernetes_virginia.pem	ubuntu@172.31.2.60 "nolimit $i >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "find data/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "rm data/*"
done
