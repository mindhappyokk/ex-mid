sudo tc qdisc del dev ens5 root

echo "Fix 1 Latency no limit"

for i in 1 2 3 
do
 START=$(date +%s.%N)
 ./migrator.sh > 1-1ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate fix 1 no limit latency" $DIFF
 echo "Finish migration"
 echo $i " Time fix 1 no limit latency" $DIFF  >> 1-1ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelost1-1ms-$i.txt
 ssh -i kubernetes_virginia.pem	ubuntu@172.31.2.60 "nolimit $i >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "find data/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "rm data/*"
done
