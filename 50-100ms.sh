sudo tc qdisc del dev ens5 root
sudo tc qdisc add dev ens5 root netem delay 100ms

echo "Fix 50 Latency 100ms"

for i in 1 2 3
do
 START=$(date +%s.%N)
 ./migrator.sh > 100-100ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate Fix 50 latency 100ms" $DIFF
 echo "Finish migration"
 echo $i " Time Fix 100 latency 50ms" $DIFF  >> 50-100ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelost50-100ms-$i.txt
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "find data/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum >> checksum-fix50.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "rm data/*"
done
