sudo tc qdisc del dev ens5 root

echo "Enhanced Latency no limit"

for i in 1 2 3 
do
 START=$(date +%s.%N)
 ./migrator.sh > enhance-1ms-$i.log
 END=$(date +%s.%N)
 DIFF=$(echo "$END - $START" | bc)
 echo "Time to migrate enhance no limit latency" $DIFF
 echo "Finish migration"
 echo $i " Time enhance no limit latency" $DIFF  >> enhance-1ms.txt
 rm output/*
 mv lost/filelost.txt lost/filelostenhand-1ms-$i.txt
# ssh suchanat@flynn.sci.tu.ac.th "rm data/*"
 ssh -i kubernetes_virginia.pem	ubuntu@172.31.2.60 "nolimit $i >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "find data/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum >> checksum.txt"
 ssh -i kubernetes_virginia.pem ubuntu@172.31.2.60 "rm data/*"
done
